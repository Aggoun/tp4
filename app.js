/*$(document).ready(function(){

    // Debut de votre code

    alert("Hello");
    $('.pizza-type label').hover(
        function(){
            $(this).find(".description").show();
        },
        function(){
            $(this).find(".description").hide();
        },
    )
});*/

/*function.showHideDescription(){
    $(this).find(".description").toggle();
}*/

$(document).ready(function(){
    function updateprix() {
        let prix = 0;

        prix += +$("input[name='type']:checked").data("price") || 0;
        prix += +$("input[name='pate']:checked").data("price") || 0;
        $("input[name='extra']:checked").each(function(i, elem) {
            prix += $(elem).data("price");
        });

        prix *= $(".nb-parts input").val() / 6;

        $(".stick-right p").text(prix + " €");
    }

    $('.pizza-type label').hover(
    function(){
        $(this).find(".description").show();
    },
    function(){
        $(this).find(".description").hide();
    },
    );

    $('.nb-parts input').on('keyup', function() {
        $(".pizza-pict").remove();
        var pizza = $('<span class="pizza-pict"></span>');
        slices = +$(this).val();

        for(i = 0; i < slices/6 ; i++) {
            $(this).after(pizza.clone().addClass('pizza-6'));
        }

        if(slices%6 != 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-'+slices%6);

        updateprix();
    });

    $(".next-step" ).click( function() {
        $(this).remove();
        $(".infos-client").slideDown();
    });

    $(".add" ).click( function() {
        //$(".infos-client .type:nth-child(2) input").append("<input type='text'/>");

        $("<input type='text'/>").insertAfter(".infos-client .type:nth-child(2) input");

        //$(".infos-client .add").before("<input type='text'/>");

    });

    $("input[data-price]").change(updateprix);

    $(".done" ).click( function() {
        var name = $(".infos-client .type:first input").val();

        $(".main").empty()
            .append("<p> Merci " + name + " votre commande sera livrée dans 15 minutes.</p>");
    });
});
